#include <iostream>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "rs232.h"
#include <unistd.h>

#include "RoboteqDevice.h"
#include "ErrorCodes.h"
#include "Constants.h"

using namespace std;

int main(int argc, char *argv[])
{

//FUSION DES EXEMPLES DES LIB RS232 ET DE ROBOTEQ
  int i=0,
      cport_nr=16,        /* /dev/ttyUSB*/
      bdrate=9600;       /* 9600 baud */

  char mode[]={'8','N','1',0},
       str[2][512];

  // ALTERNANCE ENTRE 2 ANGLES 50 ET 150 °
  strcpy(str[0], "50\n\n");

  strcpy(str[1], "150\n\n");


  if(RS232_OpenComport(cport_nr, bdrate, mode))
  {
    printf("Can not open comport\n");

    return(0);
  }


//CONNEXION AU CONTROLEUR ROBOTEQ
string response = "";
RoboteqDevice device;
int status = device.Connect("/dev/ttyS0");


//BOUCLE D ESSAI
while(1)
{
	if(status != RQ_SUCCESS)
	{
		cout<<"Error connecting to device: "<<status<<"."<<endl;
		return 1;
	}

	cout<<"- SetConfig(_DINA, 1, 1)...";
	if((status = device.SetConfig(_DINA, 1, 1)) != RQ_SUCCESS)
		cout<<"failed --> "<<status<<endl;
	else
		cout<<"succeeded."<<endl;

	//Wait 10 ms before sending another command to device
	sleepms(10);

	int result;
	cout<<"- GetConfig(_DINA, 1)...";
	if((status = device.GetConfig(_DINA, 1, result)) != RQ_SUCCESS)
		cout<<"failed --> "<<status<<endl;
	else
		cout<<"returnedf --> "<<result<<endl;

	//Wait 10 ms before sending another command to device
	sleepms(10);

	cout<<"- GetValue(_ANAIN, 1)...";
	if((status = device.GetValue(_ANAIN, 1, result)) != RQ_SUCCESS)
		cout<<"failed --> "<<status<<endl;
	else
		cout<<"returned --> "<<result<<endl;

	//Wait 10 ms before sending another command to device
	sleepms(10);

	cout<<"- SetCommand(_GO, 1, 600)...";
	if((status = device.SetCommand(_GO, 1, 600)) != RQ_SUCCESS)
		cout<<"failed --> "<<status<<endl;
	else
		cout<<"succeeded."<<endl;

    RS232_cputs(cport_nr, str[i]);

    printf("sent: %s\n", str[i]);

    usleep(1000000);  /* sleep for 1 Second */
    i++;
    i %= 2;
}

device.Disconnect();
return 0;
}
