CC=g++


all: sample.o sample_servo.o RoboteqDevice.o RS232.o

sample.o: RoboteqDevice.o
	$(CC) RoboteqDevice.o sample.cpp -o sample.o

sample_servo.o: RoboteqDevice.o rs232.o
	$(CC) RoboteqDevice.o rs232.o sample_servo_serial.cpp -o sample_servo.o

RoboteqDevice.o: RoboteqDevice.cpp
	$(CC) -c RoboteqDevice.cpp

RS232.o: rs232.c
	$(CC) -c rs232.c

clean:
	rm *.o
